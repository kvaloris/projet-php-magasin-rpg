<?php
    // Pour charger automatiquement les classes requises
    spl_autoload_register(function ($className) { @include "$className.php"; });
    
        $bd = Database::getInstance(); //paramètres connexion à la bd dans le fichier Database.php
        $param['id_classe'] =$_POST['category']; //récupérer l'id via ajax

        $stmt = $bd->prepare("SELECT objet.libelle as nom, prix, type.libelle as type
                                FROM (objet join type using (id_type)) join classe using(id_classe) 
                                WHERE id_classe=:id_classe");
        $stmt->execute($param);
        $row=$stmt->fetchAll();

        echo json_encode($row);
?>