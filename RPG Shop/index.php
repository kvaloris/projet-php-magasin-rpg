<!--test-->

<!DOCTYPE html>

<html>

<head>
    <meta charset="utf-8">
    <title>Boutique RPG</title>
    <link rel="stylesheet" type="text/css" href="style.css" />
    
</head>

<body>
    <div class="player_info">
        <p class="pseudo">Pseudo</p> <!--A remplacer par le pseudo entré-->
        <p class="money">10 000</p> <!--Augmenté d'une somme ajoutée quotidienement-->
    </div>

    
    <div class="middle" id="shop">
        <h1>Boutique</h1>
        <div class="categories">
            <h2 class="weapons">Armes</h2>
            <h2 class="armors">Armures</h2>
            <h2 class="accessories">Accessoires</h2>
            <h2 class="consum">Consommables</h2>
        </div>
        <div class="content">

        </div>
    </div>

    <!--display: none AU DÉBUT-->
    <!--(A FAIRE) SI ON CLIQUE SUR LE BOUTON .switch, AFFICHE #inventory ET CACHE #shop-->
    <div class="middle" id="inventory">
        <h1>Inventaire</h2>
        <div class="content">
            
        </div>
    </div>

    <button class="switch">inventaire</button>

<script language="javascript" type="text/javascript" src="fct_js.js"></script>

</body>

</html>