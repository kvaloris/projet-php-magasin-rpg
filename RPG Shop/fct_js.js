let button = document.querySelector('.switch');
const shop = document.querySelector('#shop');
const inventory = document.querySelector('#inventory');

function switchInventoryShop() {
    if(shop.style.display == "none") {
        shop.style.display = "block";
        inventory.style.display = "none";
    }
    else {
        shop.style.display = "none";
        inventory.style.display = "block";
    }
}
button.addEventListener('click', switchInventoryShop);

const weapons = document.querySelector('.weapons');
const armors = document.querySelector('.armors');
const accessories = document.querySelector('.accessories');
const consum = document.querySelector('.consum');

function fillCategory(category) {

    const form = new FormData();
    form.append('category', category);

    fetch( 'display.php', {
        method: "POST",
        body: form
    }) 	
    .then(response => response.json())
    .then(data=> {
        // A compléter
        let content_shop = document.querySelector('#shop > .content');
        let text ="";

        for( let i=0 ; i<data.length ; i++ ) {
            text += data[i]["nom"] + " vaut " + data[i]["prix"] + " et est un.e " + data[i]["type"] + ". <br>";
        }
        content_shop.innerHTML = text;
        console.log(data);
    })
    .catch(error => { console.log(error) });
}

weapons.addEventListener('click', (event) => {
    event.preventDefault();
    fillCategory('1');
});
armors.addEventListener('click', (event) => {
    event.preventDefault();
    fillCategory('3');
});
accessories.addEventListener('click', (event) => {
    event.preventDefault();
    fillCategory('4');
});
consum.addEventListener('click', (event) => {
    event.preventDefault();
    fillCategory('2');
});
